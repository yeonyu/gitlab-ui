import description from './new_dropdown_header.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-header',
};
