import description from './breadcrumb.md';
import examples from './examples';

export default {
  followsDesignSystem: false,
  examples,
  description,
  bootstrapComponent: 'b-breadcrumb',
};
