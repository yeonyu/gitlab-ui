import * as description from './avatars_inline.md';
import examples from './examples';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
