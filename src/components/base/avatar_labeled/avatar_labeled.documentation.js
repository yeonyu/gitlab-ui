import description from './avatar_labeled.md';
import examples from './examples';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
