import description from './loading_icon.md';
import examples from './examples';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
