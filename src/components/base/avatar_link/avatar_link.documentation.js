import description from './avatar_link.md';
import examples from './examples';

export default {
  followsDesignSystem: false,
  description,
  examples,
};
