import examples from './examples';

export default {
  followsDesignSystem: false,
  examples,
};
