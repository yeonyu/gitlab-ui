# frozen_string_literal: true

require 'digest/md5'

GITLAB_PROJECT_ID = 'gitlab'

MESSAGE = <<~MARKDOWN
  ## Reviewer roulette

  Changes that require review have been detected! A merge request is normally
  reviewed by both a reviewer and a maintainer in its primary category (e.g.
  ~frontend or ~backend), and by a maintainer in all other categories.
MARKDOWN

CATEGORY_TABLE_HEADER = <<~MARKDOWN

  To spread load more evenly across eligible reviewers, Danger has randomly picked
  a candidate for each review slot. Feel free to override this selection if you
  think someone else would be better-suited, or the chosen person is unavailable.

  Once you've decided who will review this merge request, mention them as you
  normally would! Danger does not (yet?) automatically notify them for you.

  | Category | Reviewer | Maintainer |
  | -------- | -------- | ---------- |
MARKDOWN

UNKNOWN_FILES_MESSAGE = <<~MARKDOWN

  These files couldn't be categorised, so Danger was unable to suggest a reviewer.
  Please consider creating a merge request to
  [add support](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/lib/gitlab/danger/helper.rb)
  for them.
MARKDOWN

NO_REVIEWER = 'No reviewer available'
NO_MAINTAINER = 'No maintainer available'

def team_for_project(project)
  roulette.project_team(project)
rescue StandardError => e
  warn("Reviewer roulette failed to load #{project} team data: #{e.message}")
  []
end

def spin_for_category(team, project, category, branch_name)
  random = roulette.new_random(branch_name)

  reviewers, traintainers, maintainers =
    %i[reviewer? traintainer? maintainer?].map do |kind|
      team.select do |member|
        member.public_send(kind, project, category)
      end
    end

  # Make traintainers have triple the chance to be picked as a reviewer
  reviewer = roulette.spin_for_person(reviewers + traintainers + traintainers, random: random)
  maintainer = roulette.spin_for_person(maintainers, random: random) if category != :docs

  {
    "label": helper.label_for_category(category),
    "reviewer": reviewer&.markdown_name,
    "maintainer": maintainer&.markdown_name
  }
end

changes = helper.changes_by_category
categories = changes.keys

if changes.any?
  branch_name = gitlab.mr_json['source_branch']

  team = team_for_project(GITLAB_PROJECT_ID)

  rows = categories.map do |category|
    spin_for_category(team, GITLAB_PROJECT_ID, category, branch_name)
  end

  # If these are docs-only changes, we need to pick a frontend maintainer for the final review
  if categories == %i[docs]
    frontend_row = spin_for_category(team, GITLAB_PROJECT_ID, :frontend, branch_name)
    # We remove the frontend reviewer as the initial review will be performed by a technical writer
    frontend_row[:reviewer] = nil
    rows << frontend_row
  end

  roulette_rows = rows.map do |row|
    "| #{row[:label]} | #{row[:reviewer] || NO_REVIEWER} | #{row[:maintainer] || NO_MAINTAINER} |"
  end

  markdown(MESSAGE)
  markdown(CATEGORY_TABLE_HEADER + roulette_rows.join("\n"))
end
